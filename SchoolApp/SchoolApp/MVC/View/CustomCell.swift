//
//  CustomCell.swift
//  SchoolApp
//
//  Created by G RaviTeja on 18/03/23.
//

import UIKit

class CustomCell: UITableViewCell {
    @IBOutlet weak var dbnLabel: UILabel!
    @IBOutlet weak var schoolNameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
