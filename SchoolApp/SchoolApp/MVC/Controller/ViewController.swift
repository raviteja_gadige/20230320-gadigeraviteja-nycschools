//
//  ViewController.swift
//  SchoolApp
//
//  Created by G RaviTeja on 18/03/23.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var schoolsArray = [SchoolModel]()
    var searchSchoolsArray = [SchoolModel]()
    var searching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchBar.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        schoolsDataAPI ()
    }
    
    
    func schoolsDataAPI () {
        let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else { return }
            do {
                
                let array = try JSONDecoder().decode([SchoolModel].self, from: data)
                self.schoolsArray = array.sorted {
                    $0.school_name ?? "" < $1.school_name ?? ""
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            } catch {
                print(error)
            }
            
            
//            self.schoolsArray = parseJsonData(data: data)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
            
        }
        task.resume()
    }
}

extension ViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchSchoolsArray = schoolsArray.filter{ ($0.school_name?.lowercased().prefix(searchText.count))! == searchText.lowercased()}
        searching = true
        tableView.reloadData()
        
    }
    
}


extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searching {
            return searchSchoolsArray.count
        } else {
            return schoolsArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomCell
        
        if searching {
            cell.dbnLabel.text = searchSchoolsArray[indexPath.row].dbn
            cell.schoolNameLabel.text = searchSchoolsArray[indexPath.row].school_name
        } else {
            cell.dbnLabel.text = schoolsArray[indexPath.row].dbn
            cell.schoolNameLabel.text = schoolsArray[indexPath.row].school_name
        }
        
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedSchool: SchoolModel?
        
        if searching {
            selectedSchool = searchSchoolsArray[indexPath.row]
        } else {
            selectedSchool = schoolsArray[indexPath.row]
        }
        
        self.searchBar.searchTextField.endEditing(true)
        
        let detailVC = storyboard?.instantiateViewController(withIdentifier: "SchoolDetailsVC") as! SchoolDetailsViewController
        detailVC.schoolObj = selectedSchool
        navigationController?.pushViewController(detailVC, animated: true)
    }
}

