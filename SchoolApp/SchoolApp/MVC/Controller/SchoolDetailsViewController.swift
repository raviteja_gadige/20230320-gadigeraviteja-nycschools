//
//  SchoolDetailsViewController.swift
//  SchoolApp
//
//  Created by G RaviTeja on 19/03/23.
//

import UIKit

class SchoolDetailsViewController: UIViewController {
    
    var schoolObj:SchoolModel?
        
    @IBOutlet weak var schoolNameLabel: UILabel!
    
    @IBOutlet weak var dbnLabel: UILabel!
    
    @IBOutlet weak var numOfSatLabel: UILabel!
    
    @IBOutlet weak var satMatLabel: UILabel!
    
    @IBOutlet weak var satWritingAvgLabel: UILabel!
    
    @IBOutlet weak var satCriticalLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        schoolNameLabel.text = schoolObj?.school_name
        dbnLabel.text = schoolObj?.dbn
        numOfSatLabel.text = schoolObj?.num_of_sat_test_takers
        satMatLabel.text = schoolObj?.sat_math_avg_score
        satWritingAvgLabel.text = schoolObj?.sat_writing_avg_score
        satCriticalLabel.text = schoolObj?.sat_critical_reading_avg_score
       
        
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
}

